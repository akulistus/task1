public class main
{
    public static void main(String[] args)
    {
        int gradeBook = 0xE8024;
        long phoneNumber = 89119511685L;
        int lastTwoPhoneDigis = 0b1010101;
        int lastFourPhoneDigis = 03225;

        int studentPosition = 14;
        int key = ((studentPosition-1) % 26) + 1;
        char c = (char) (key+64);
        char sym = Character.toUpperCase(c);

        System.out.println(gradeBook);
        System.out.println(phoneNumber);
        System.out.println(lastTwoPhoneDigis);
        System.out.println(lastFourPhoneDigis);
        System.out.println(key);
        System.out.println(sym);
    }
}